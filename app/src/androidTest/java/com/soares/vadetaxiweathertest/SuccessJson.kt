package com.soares.vadetaxiweathertest

object SuccessJson {

    const val json =
            """{
	                "coord": {
		            "lon": -67.14,
		            "lat": -27.71
                    },
                    "weather": [{
                        "id": 800,
                        "main": "Clear",
                        "description": "céu claro",
                        "icon": "01d"
                    }],
                    "base": "stations",
                    "main": {
                        "temp": 13.17,
                        "pressure": 795.05,
                        "humidity": 58,
                        "temp_min": 13.17,
                        "temp_max": 13.17,
                        "sea_level": 1024.39,
                        "grnd_level": 795.05
                    },
                    "wind": {
                        "speed": 0.86,
                        "deg": 92.0027
                    },
                    "clouds": {
                        "all": 0
                    },
                    "dt": 1533503594,
                    "sys": {
                        "message": 0.0019,
                        "country": "AR",
                        "sunrise": 1533467202,
                        "sunset": 1533506549
                    },
                    "id": 3846616,
                    "name": "Londres",
                    "cod": 200
                }"""
}