package com.soares.vadetaxiweathertest.espresso

import android.support.annotation.StringRes
import android.support.test.espresso.Espresso
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers

fun Int.onView() =
        Espresso.onView(ViewMatchers.withId(this))

fun ViewInteraction.checkText(text: String) =
        check(ViewAssertions.matches(ViewMatchers.withText(text)))

fun ViewInteraction.checkText(@StringRes text: Int) =
        check(ViewAssertions.matches(ViewMatchers.withText(text)))

fun ViewInteraction.isCompletelyShown(): ViewInteraction =
        check(ViewAssertions.matches(ViewMatchers.isCompletelyDisplayed()))

fun ViewInteraction.type(content: String): ViewInteraction =
        perform(ViewActions.typeText(content))

fun ViewInteraction.click(): ViewInteraction =
        perform(ViewActions.click())