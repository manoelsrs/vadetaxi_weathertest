package com.soares.vadetaxiweathertest

import android.support.annotation.StringRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.soares.vadetaxiweathertest.core.http.VadetaxiApi
import com.soares.vadetaxiweathertest.core.utils.getTime
import com.soares.vadetaxiweathertest.espresso.*
import com.soares.vadetaxiweathertest.main.WeatherActivity
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.SocketPolicy
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WeatherActivityTest {

    @Rule
    @JvmField
    val rule = IntentsTestRule(
            WeatherActivity::class.java,
            false,
            false
    )

    private val server = MockWebServer()

    private fun launchActivity() {
        rule.launchActivity(null)
    }

    @Before
    fun before() {
        server.start()
        VadetaxiApi.setUrl(server.url("/").toString())
    }

    @After
    fun after() {
        server.shutdown()
    }

    @Test
    fun testNormalFlow() {
        startServer(listOf(MockResponse().setBody(SuccessJson.json)))
        launchActivity()

        R.id.searchET.onView()
                .type("Londres")

        R.id.searchBT.onView()
                .checkText(R.string.searchBT_text)
                .checkText("PROCURAR")
                .click()

        R.id.cityTV.onView()
                .checkText("Londres/AR")
                .isCompletelyShown()

        R.id.minTempTV.onView()
                .checkText("13⁰C")
                .isCompletelyShown()

        R.id.tempTV.onView()
                .checkText("13⁰C")
                .isCompletelyShown()

        R.id.maxTempTV.onView()
                .checkText("13⁰C")
                .isCompletelyShown()

        R.id.descriptionTV.onView()
                .checkText("céu claro".toUpperCase())
                .isCompletelyShown()

        R.id.sunriseTV.onView()
                .checkText(1533467202L.getTime())
                .isCompletelyShown()

        R.id.sunsetTV.onView()
                .checkText(1533506549L.getTime())
                .isCompletelyShown()
    }

    @Test
    fun testServerError_404_Flow() {
        startServer(listOf(MockResponse().setResponseCode(404)))
        launchActivity()

        R.id.searchET.onView()
                .type("Londres")

        R.id.searchBT.onView()
                .checkText(R.string.searchBT_text)
                .checkText("PROCURAR")
                .click()

        checkToastView(R.string._404)
    }

    @Test
    fun testServerError_4XX_Flow() {
        startServer(listOf(MockResponse().setResponseCode(400)))
        launchActivity()

        R.id.searchET.onView()
                .type("Londres")

        R.id.searchBT.onView()
                .checkText(R.string.searchBT_text)
                .checkText("PROCURAR")
                .click()

        checkToastView(R.string._400_499)
    }

    @Test
    fun testServerError_5XX_Flow() {
        startServer(listOf(MockResponse().setResponseCode(500)))
        launchActivity()

        R.id.searchET.onView()
                .type("Londres")

        R.id.searchBT.onView()
                .checkText(R.string.searchBT_text)
                .checkText("PROCURAR")
                .click()

        checkToastView(R.string._500)
    }

    @Test
    fun testServerError_timeout_Flow() {
        startServer(listOf(MockResponse().setSocketPolicy(SocketPolicy.NO_RESPONSE)))
        launchActivity()

        R.id.searchET.onView()
                .type("Londres")

        R.id.searchBT.onView()
                .checkText(R.string.searchBT_text)
                .checkText("PROCURAR")
                .click()

        checkToastView(R.string.noConnection)
    }

    private fun checkToastView(@StringRes error: Int) {
        onView(withText(error)).inRoot(withDecorView(not(rule.activity.window.decorView)))
                .check(matches(isCompletelyDisplayed()))
    }

    private fun startServer(mockResponse: List<MockResponse>) {
        mockResponse.forEach {
            server.enqueue(it)
        }
    }
}