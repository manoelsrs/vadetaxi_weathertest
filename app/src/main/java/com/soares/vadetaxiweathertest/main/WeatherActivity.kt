package com.soares.vadetaxiweathertest.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.soares.vadetaxiweathertest.R
import dagger.android.AndroidInjection
import javax.inject.Inject

class WeatherActivity : AppCompatActivity() {

    @Inject
    lateinit var weatherController: WeatherController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)
        AndroidInjection.inject(this)

        weatherController.onCreate()
    }

    override fun onDestroy() {
        weatherController.onDestroy()
        super.onDestroy()
    }
}
