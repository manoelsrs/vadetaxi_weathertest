package com.soares.vadetaxiweathertest.main

import android.view.View
import com.soares.vadetaxiweathertest.main.viewmodel.WeatherViewModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.activity_weather.view.*
import java.lang.Exception

class WeatherView(private val rootView: View) {

    private val searchBTListener = BehaviorSubject.create<String>()
    fun getSearchBTListener(): Observable<String> = searchBTListener

    fun onCreate() {
        rootView.apply {
            searchBT.setOnClickListener {
                searchBTListener.onNext(searchET.text.toString())
            }
        }
    }

    fun showProgress(show: Boolean) {
        rootView.apply {
            progressLayout.visibility =
                    if (show) View.VISIBLE
                    else View.GONE

            weatherResultLayout.visibility =
                    if (!show) View.VISIBLE
                    else View.GONE
        }
    }

    fun backToInit() {
        rootView.progressLayout.visibility = View.GONE
    }

    fun showAnswer(answer: WeatherViewModel) {
        rootView.apply {
            cityTV.text = answer.city
            showIcon(answer.iconUrl)
            minTempTV.text = answer.minTemp
            tempTV.text = answer.temp
            maxTempTV.text = answer.maxTemp
            descriptionTV.text = answer.description
            humidityTV.text = answer.humidity
            sunriseTV.text = answer.sunrise
            sunsetTV.text = answer.sunset
        }
    }

    private fun showIcon(url: String) {
        rootView.apply {
            Picasso.get()
                    .load(url)
                    .into(iconIV, object : Callback {
                        override fun onSuccess() {
                            iconIV.visibility = View.VISIBLE
                            imagePB.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            imageLayout.visibility = View.GONE
                        }

                    })
        }
    }
}