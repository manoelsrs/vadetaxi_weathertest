package com.soares.vadetaxiweathertest.main.viewmodel

interface AnswerViewModel {
    val raw: Any
    val city: String
    val icon: String
    val iconUrl: String
    val minTemp: String
    val temp: String
    val maxTemp: String
    val description: String
    val humidity: String
    val sunrise: String
    val sunset: String
}