package com.soares.vadetaxiweathertest.main

import android.app.Activity
import com.soares.vadetaxiweathertest.R
import com.soares.vadetaxiweathertest.core.http.VadetaxiApi
import com.soares.vadetaxiweathertest.core.http.friendlyMessage
import com.soares.vadetaxiweathertest.core.scheduler.RxScheduler
import com.soares.vadetaxiweathertest.main.viewmodel.WeatherViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.toast
import retrofit2.HttpException

class WeatherController(
        private val view: WeatherView,
        private val scheduler: RxScheduler,
        private val activity: Activity,
        private val searchBTListener: Observable<String>
) {

    private val disposable = CompositeDisposable()

    fun onCreate() {
        view.apply {
            onCreate()
            disposable.add(
                    searchBTListener.map { it.trim() }.subscribe { search(it) }
            )
        }
    }

    fun onDestroy() {
        disposable.clear()
    }

    private fun search(city: String) {
        disposable.add(
                VadetaxiApi
                        .getWeather(city)
                        .subscribeOn(scheduler.backgroundThread())
                        .observeOn(scheduler.mainThread())
                        .doOnSubscribe { view.showProgress(true) }
                        .doOnComplete { view.showProgress(false) }
                        .subscribe ({
                            view.showAnswer(it)
                        },{
                            when(it) {
                                is HttpException -> it.friendlyMessage()
                                else -> R.string.noConnection
                            }.run { showErrorMessage(this) }

                            view.backToInit()
                        })
        )
    }

    private fun showErrorMessage(message: Int) = activity.toast(message)
}