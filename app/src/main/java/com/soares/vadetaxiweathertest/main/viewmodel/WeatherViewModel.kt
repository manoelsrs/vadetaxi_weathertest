package com.soares.vadetaxiweathertest.main.viewmodel

import com.soares.vadetaxiweathertest.core.utils.getTime
import com.soares.vadetaxiweathertest.model.Model

data class WeatherViewModel(private val answer: Model.Answer) : AnswerViewModel {
    override val raw: Any = answer
    override val city: String = "${answer.city}/${answer.plus.country}"
    override val icon: String = answer.weather[0].icon
    override val iconUrl: String = "http://openweathermap.org/img/w/$icon.png"
    override val minTemp: String = "${answer.temperatures.minTemp.toInt()}⁰C"
    override val temp: String = "${answer.temperatures.temp.toInt()}⁰C"
    override val maxTemp: String = "${answer.temperatures.maxTemp.toInt()}⁰C"
    override val description: String = answer.weather[0].description.toUpperCase()
    override val humidity: String = "${answer.temperatures.humidity}%"
    override val sunrise: String = answer.plus.sunrise.getTime()
    override val sunset: String = answer.plus.sunset.getTime()
}