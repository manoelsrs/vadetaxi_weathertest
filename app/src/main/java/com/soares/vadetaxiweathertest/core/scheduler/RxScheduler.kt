package com.soares.vadetaxiweathertest.core.scheduler

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RxScheduler {

    fun mainThread(): Scheduler = AndroidSchedulers.mainThread()

    fun backgroundThread(): Scheduler = Schedulers.io()
}