package com.soares.vadetaxiweathertest.core.http

import com.soares.vadetaxiweathertest.model.Model
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface Service {

    @GET("weather")
    fun getWeather(
            @Query("q") city: String,
            @Query("appid") key: String = "3a254f77df5d9c5a09ff33278dcd79f1",
            @Query("units") units: String = "metric",
            @Query("lang") lang: String = "pt"
    ): Observable<Model.Answer>
}