package com.soares.vadetaxiweathertest.core.http

import com.soares.vadetaxiweathertest.R
import retrofit2.HttpException

fun HttpException.friendlyMessage() =
        when (code()) {
            404 -> R.string._404

            in 400..499 -> R.string._400_499

            in 500..599 -> R.string._500

            else -> R.string.noConnection
        }