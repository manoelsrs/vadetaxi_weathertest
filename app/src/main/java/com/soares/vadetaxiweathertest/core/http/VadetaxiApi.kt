package com.soares.vadetaxiweathertest.core.http

import android.support.annotation.VisibleForTesting
import com.google.gson.GsonBuilder
import com.soares.vadetaxiweathertest.main.viewmodel.WeatherViewModel
import com.soares.vadetaxiweathertest.model.Model
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object VadetaxiApi {

    private const val BASE_URL = "http://api.openweathermap.org/data/2.5/"

    private var service: Service
    private val interceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    init {
        service = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient(interceptor))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                .build()
                .create(Service::class.java)
    }

    private fun okHttpClient(interceptor: HttpLoggingInterceptor) =
            OkHttpClient.Builder()
                    .connectTimeout(30,TimeUnit.SECONDS)
                    .readTimeout(30,TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build()

    @VisibleForTesting
    fun setUrl(url: String) {
        service = Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient(interceptor))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                .build()
                .create(Service::class.java)
    }

    fun getWeather(city: String): Observable<WeatherViewModel> = service
                    .getWeather(city)
                    .map { WeatherViewModel(it) }

}