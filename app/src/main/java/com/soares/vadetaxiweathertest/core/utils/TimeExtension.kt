package com.soares.vadetaxiweathertest.core.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Long.getTime(): String {
    val date = Date(this * 1000L)
    val simpleDateFormat = SimpleDateFormat("HH:mm")
    simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT-3")

    return "${simpleDateFormat.format(date)} - GMT-3"
}