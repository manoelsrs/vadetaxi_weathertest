package com.soares.vadetaxiweathertest.di.module

import android.app.Application
import com.soares.vadetaxiweathertest.WeatherApp
import com.soares.vadetaxiweathertest.core.scheduler.RxScheduler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplication(app: WeatherApp): Application = app

    @Provides
    @Singleton
    fun provideRxScheduler(): RxScheduler = RxScheduler()
}