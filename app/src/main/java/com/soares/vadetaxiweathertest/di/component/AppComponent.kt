package com.soares.vadetaxiweathertest.di.component

import com.soares.vadetaxiweathertest.WeatherApp
import com.soares.vadetaxiweathertest.di.builder.ActivityBuilder
import com.soares.vadetaxiweathertest.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    ActivityBuilder::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: WeatherApp): Builder

        fun build(): AppComponent
    }

    fun inject(app: WeatherApp)
}