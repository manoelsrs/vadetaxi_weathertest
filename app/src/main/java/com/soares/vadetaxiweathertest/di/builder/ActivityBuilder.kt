package com.soares.vadetaxiweathertest.di.builder

import com.soares.vadetaxiweathertest.di.PerActivity
import com.soares.vadetaxiweathertest.di.module.WeatherActivityModule
import com.soares.vadetaxiweathertest.main.WeatherActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [(WeatherActivityModule::class)])
    abstract fun bindWeatherActivity(): WeatherActivity
}