package com.soares.vadetaxiweathertest.di.module

import com.soares.vadetaxiweathertest.core.scheduler.RxScheduler
import com.soares.vadetaxiweathertest.di.PerActivity
import com.soares.vadetaxiweathertest.main.WeatherActivity
import com.soares.vadetaxiweathertest.main.WeatherController
import com.soares.vadetaxiweathertest.main.WeatherView
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_weather.*

@Module
class WeatherActivityModule {

    @PerActivity
    @Provides
    fun provideWeatherController(
            view: WeatherView,
            scheduler: RxScheduler,
            activity: WeatherActivity
    ) =
            WeatherController(
                    view,
                    scheduler,
                    activity,
                    view.getSearchBTListener()
            )

    @PerActivity
    @Provides
    fun provideUI(activity: WeatherActivity) = WeatherView(activity.weatherLayout)
}