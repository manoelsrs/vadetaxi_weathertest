package com.soares.vadetaxiweathertest.model

import com.google.gson.annotations.SerializedName

object Model {

    data class Answer(
            val weather: List<Weather>,
            @SerializedName("main") val temperatures: Temperatures,
            @SerializedName("sys") val plus: Plus,
            @SerializedName("name") val city: String,
            @SerializedName("cod") val serverAnswer: Int
    )

    data class Weather(
            @SerializedName("id") val imageCode: Int,
            val description: String,
            val icon: String
    )

    data class Temperatures(
            val temp: Double,
            val humidity: Double,
            @SerializedName("temp_min") val minTemp: Double,
            @SerializedName("temp_max") val maxTemp: Double
    )

    data class Plus(
            val country: String,
            val sunrise: Long,
            val sunset: Long
    )
}