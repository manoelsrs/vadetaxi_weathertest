package com.soares.vadetaxiweathertest.model

import com.soares.vadetaxiweathertest.mock.SuccessMock
import org.junit.Assert.*
import org.junit.Test

class ModelTest {

    private val modelMock = SuccessMock.model

    @Test
    fun testSuccess() {

        val answer = Model.Answer(
                listOf(Model.Weather(800, "céu claro", "01d")),
                Model.Temperatures(13.17, 58.0, 13.17, 13.17),
                Model.Plus("AR",1533467202, 1533506549),
                "Londres",
                200
        )

        assertEquals(answer,modelMock)
    }
}